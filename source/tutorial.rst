.. This file is part of qmesh synoptic manual
   Copyright (C) 2017 Alexandros Avdis.
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _Tutorial:

Tutorial
==========================

A Crash course in GIS
--------------------------

If you are an expert in GIS, you can skim this section.
However, do not completely skip it, as we have included here a number of tips and tricks with GIS, that will save you a lot or time and effort when creating new meshes.


The GIS software we have used in this section is QGIS, because it is open-source and free, thus it is accesible to everyone.
However, the concepts are applicable to any GIS suite.
Indeed, the two most fundamental datatypes in GIS are *vector* and *raster* data, and their correspondence to mesh-generator data-structures was discussed in the :ref:`Introduction` and in :cite:`qmesh:2018`. 


Lets examine an example of each data-structure. Create a folder for the purpose of this example, named ``GIS_crash_course`` and inside that place the following two files:

* The filtered and subsampled GEBCO global bathymetry, which you can download from `figshare <>`_.
This is an example of a raster file, where the global bathymetry and topography is represented as data over a regular grid: a number of points along the two geodetic coordinates, longitude and latitude define "pixels" and values attached to each of these points give the value of the elevation at that point.

* The global 0-meter elevation contour, which you can download from `figshare <>`_.
This is an example of a vector file, where a sequence of points defines a line.

Place both files inside the ``GIS_crash_course`` folder, and open QGIS. You should have a window similar to :numref:`figureQgisWindowAtStartUp`, below.

.. _figureQgisWindowAtStartUp:
.. figure:: figures/qgis_at_start.png
   :align: center

   The Qgis Window.

Lets examine the major areas and tools highlighted in figure :numref:`figureQgisWindowAtStartUp`, starting with the *canvas*, and *layers* windows.
Each vector or raster file is represented in GIS as a separate *layer*.
The purpose of the *canvas* is to overlay all layers, and facilitate contextual comparison and editing of the layers.
Lets add the files we downloaded to see how the canvas and layers windows change.
You can add the bathymetry via the ``Add Raster Layer`` button, also highlighted in :numref:`figureQgisWindowAtStartUp`.

.. editing shapefiles (moving, removing points, moving removing features)

.. extracting contours from rasters

.. vector attribute editing and creation. The length attribute and its usefulness. The PhysID attribute.

Global meshes
--------------------------

Meshing the Mediterranean Sea
--------------------------------

Meshing the North Sea
--------------------------

Meshing the Severn Estuary
----------------------------

Meshing the Seas around the Orkney and Shetland Islands
---------------------------------------------------------
