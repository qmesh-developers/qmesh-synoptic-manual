.. _furtherDocumentation:

Further documentation
=======================

.. _API_Reference:

API Rerefence
-------------------------

CLI Rerefence
-------------------------

GUI Rerefence
-------------------------

Developer Documentation
-------------------------

.. _academic_papers:

Academic papers
-------------------------

Papers on qmesh
~~~~~~~~~~~~~~~~~~~~~~~~
* Avdis, A. & Candy A. S. & Hill J. & Kramer C. S. & Piggott M. D.,
  *"Efficient unstructured mesh generation for marine renewable energy applications"*,
  Renewable Energy, Volume 116, Part A, February 2018, Pages 842-856,
  `<https://doi.org/10.1016/j.renene.2017.09.058>`_

* Jacobs C. T. & Avdis A. & Mouradian S. L. & Piggott M. D.,
  *"Integrating Research Data Management into Geographical Information Systems"*,
  5th International Workshop on Semantic Digital Archives (SDA), 2015,
  `<http://hdl.handle.net/10044/1/28557>`_, `<http://ceur-ws.org/Vol-1529/>`_

Papers using qmesh
~~~~~~~~~~~~~~~~~~~~~~~~

* Collins D. S. & Avdis A. & Allison P. A. & Johnson H. D. & Hill J. & Piggott M. D., Hassan M. H. A. & Damit A. R.,
  *"Tidal dynamics and mangrove carbon sequestration during the Oligo–Miocene in the South China Sea"*,
  Nature Communications, Volume 8, Article number: 15698, 2017,
  `<https://doi.org/10.1038/ncomms15698>`_

* Pérez-Ortiz A. & Borthwick G. L. A. & McNaughton J. & Avdis A.,
  *"Characterization of the tidal resource in Rathlin Sound"*,
  Renewable Energy, Volume 114, Part A, December 2017, Pages 229-243,
  `<https://doi.org/10.1016/j.renene.2017.04.026>`_

.. _tutorials:

Tutorials
-------------------------

Basic invocation and initialisation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

initialisation, version numbers git sha keys in all APIs

.. _meshGenerationInnerSound:

Mesh generation for simulations of tidal flow with a turbine array in the Inner Sound 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Here we demonstrate how qmesh can be used for high-resolution simulations in the context of renewable energy generation with tidal turbine farms.
.. A mesh is constructed in a domain encompassing Northern Scotland, the Orkney and Shetland islands, as shown in


.. Building the geometry: shorelines and open boundaries.
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
A dataset containing the complete definition of the domain geometry can be downloaded from `figshare <https://doi.org/10.6084/m9.figshare.4519067>`_.
A python script that uses this data can be found in the qmesh (API core) code repository and is a good example of the qmesh API: *qmesh/examples/OrkneyShetland_UTM30/OrkneyShetland_UTM30.py* run this file with *python OrkneyShetland_UTM30.py*. You will hopefully obtain a mesh similar to the one in `figshare <https://doi.org/10.6084/m9.figshare.5350006>`_, but your mesh will not be identical to it. Differences in platforms, dependecy versions as well as differences in qmesh versions will prevent identical meshes.

