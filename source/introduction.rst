.. This file id part of qmesh synoptic manual
   Copyright (C)  2017 Alexandros Avdis.
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _Introduction:

Introduction
================

Computational models are increasingly used to study complex geophysical fluid dynamics and its interaction with biological and geochemical processes :cite:`luo_et_al:2012`.
The potential insight from simulations has made such models a valuable tool in scientific research as well as engineering.
Simulations are used to assess the impact of anthropogenic changes, the vulnerability of infrastructure to natural hazards :cite:`hunter_et_al:2008,hill:2014` and in hydrocarbon exploration and sequestration research :cite:`wells_et_al:2010,mitchell_et_al:2011,collins_et_al:2017`.
qmesh was built to facilitate one of the first stages in computational geophysical modelling: the specification of the simulation domain and its tessellation into discrete elements, commonly referred to as mesh generation. The predictive accuracy of simulations can be significantly affected by the mesh resolution, gradation and shape of mesh elements, collectively identified as “mesh quality”. Therefore, generation of high-quality meshes is fundamental to ocean and coastal modelling.

The paradigm of mesh generation in large-scale geophysical modelling can be broadly described as a two--step procedure.
In the first step, the domain is defined in a topologically two-dimensional space, bounded by geophysical contours such as shorelines and open boundaries :cite:`Gorman_et_al:2006`.
A finite, two-dimensional reference surface is thus defined, often on a geodetic datum, and a mesh is generated over this reference surface.
If a two-dimensional approximation, such as the depth-averaged shallow water equations is sufficient, mesh-generation is complete.
When a three-dimensional approximation is required, the second step is the projection of the surface mesh vertices at successive levels towards the domain floor, thus creating three-dimensional elements :cite:`Gorman_et_al:2006`.
The second step requires little user intervention, so the first step has become a synonym for mesh generation, in the large--scale geophysical modelling context.

Despite the reduction of the relevant dimensions to just two, the production of quality meshes for geophysical domains can be an elaborate procedure :cite:`Gorman_et_al:2006,Gorman_et_al:2008,heinzer:2012,berry_et_al:2014,prodanovic:2015`.
A significant number of data sources must be combined to compose a geometrically complex domain, and the geometry of geophysical domains is one of the most widely known examples of fractal geometry in nature :cite:`Mandelbrot_et_al:1983`.
Geometrical length--scales across four orders of magnitude is typical in ocean modelling: For example in ocean modelling, the simulation domain size can span hundreds of kilometres, while the smallest bays can be tens of metres long.
An even smaller scale may be relevant when the domain must accurately represent coastal infrastructure such as piers, pylons or embankments :cite:`xia_et_al:2010,avdis_etal:2016`.
In addition to domain geometry, the flow typically exhibits a large range of scales, with many transient
flow features, such as internal waves or jets, appearing due to the geometric complexity of the domain :cite:`bentley:subm_2016`. Therefore, a mesh must
represent very complex domains with element sizes across a broad range of scales, with smaller elements in areas that require a higher fidelity,
while gradation across element sizes must be smooth.
Unstructured meshes are increasingly favoured in many models as they tend to satisfy the above requirements with relative ease :cite:`danilov:2013`.

.. The bringing together of GIS and mesh generators

High--quality generic unstructured mesh generation packages are available, enabling routine generation of meshes.
However, the interface of most mesh generators is based on *Computer Aided Design and Manufacture (CAD--CAM)*.
Such interfaces have been developed for describing geometries produced by manufacturing and construction processes and do not facilitate use or manipulation of geographical data :cite:`keith_turner:2005`.
In particular, the fractal nature of ocean and coastal domains, as well as the various conventions used in geodetic coordinate reference systems are not natively expressed in CAD--CAM systems :cite:`keith_turner:2005,avdis_etal:2016`.
Combining shoreline or bathymetry data is a typical example; one may have to combine several global, national and regional data sets as well as data from very high--resolution surveys over relatively small regions.
Each data--set can also differ at least in terms of extents, resolution, coordinate reference system and vertical zero--datums.
Unlike CAD--CAM, *Geographical Information Systems (GIS)* have been developed specifically for storing and analysing geographical information.
GIS packages are robust and widely used in research as well as in operational and strategic planning contexts, where resource management, hazard mitigation and infrastructure development are a few examples.
Therefore, GIS packages are ideal for supplying mesh generators with the geospatial data they require.

.. RDM intro

Another important feature of GIS systems is their capability to interact with databases, allowing concurrent data analysis and manipulation.
Databases can also be used to record data origin and evolution, termed *data provenance* :cite:`Jacobs_etal:2015`.
However, databases in GIS systems are typically not maintained in a manner pertinent to scientific research, where the primary aim of data provenance is to show reproducibility :cite:`Jacobs_etal:2015`.
The practice of *Research Data Management (RDM)* is aimed at addressing data provenance, attribution and reproducibility.
A record of data and software depended upon is usually required as evidence of reproducibility, including scientific data and software used in preparation of simulations.
Therefore, the reproducibility of numerical simulations relies, at least in part, on the ability to exactly reproduce the underlying mesh.
The integration of RDM and mesh generation was motivated by the increased attention on the reproducibility of scientific computation :cite:`LeVeque_etal_2012`, perhaps as much as open--source software.
Also, public research bodies are adopting policies on data and software output from publicly--funded research to be made readily available, and provenance to be clearly identified :cite:`European_Commission2012-lu,European_Commission2012-cr,rcuk:2015`.
In the industrial sector reproducibility, data archiving and data provenance are viewed as efficient modelling practices.
Industry and governing bodies are also bound by regulatory frameworks which require public accesibility to data during the planning phase :cite:`directive_2003/35/EC,UKstatue_1999_No_3445,UKstatue_2007_No_1518,UKstatue_2008_No_55`, as well as after commisioning :cite:`directive_2003/4/EC,UKstatue_2004_No_3391`, especially when data pertains to environmental impact of infrastructure.

.. qmesh outilne.

Here we present the qmesh package, interfacing GIS with a mesh--generator and online data repositories.
We link the abstractions offered by mesh generators and GIS packages and build tools that facilitate mesh--generation for coastal flow modelling.
Unlike existing integrations of GIS and mesh generators, qmesh was principally developed as an object--oriented software library, accessible through an Application Programming Interface (API).
In qmesh GIS capability is implemented through the use of existing and robust GIS implementations as generic libraries, rather than building extensions to a particular GIS implementation, such as is described in :cite:`CHC:1998,Gorman_et_al:2008,heinzer:2012,berry_et_al:2014,prodanovic:2015,remacle:2016`.
*qmesh is thus a library with which command line utilities and a graphical interface have also been developed*.
The broader aim of qmesh development is the creation of robust, efficient, operational and user--friendly tools for mesh generation over geophysical domains.
The qmesh design was centred around providing the following requirements:
 * Facilitation of domain geometry and mesh element size definitions;
 * An intuitive way of specifying boundary conditions and parameterizations;
 * The ability to use various geodetic coordinate reference systems;
 * Promotion of the reproducibility of output and citation of data provenance
 * Provision of an open--source and tested package.

.. bibliography:: references.bib
   :cited:
   :style: unsrt
