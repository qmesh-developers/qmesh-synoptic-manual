.. This file is part of qmesh synoptic manual
   Copyright (C) 2017 Alexandros Avdis.
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Contents
============

| Copyright (C) 2017 Alexandros Avdis & Jon Hill
|
| Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3  or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license is included in the section entitled "GNU  Free Documentation License".
|

Welcome to the qmesh synoptic manual.

qmesh is an unstructured triangular mesh generator for geophysical domains. It is suited to mesh generation over topologically two-dimensional domains, typically used in coastal and ocean modelling. Typical examples can be seen in the :ref:`academic_papers` section.

qmesh was developed by `Alexandros Avdis <https://orcid.org/0000-0002-2695-3358>`_ and `Jon Hill <https://orcid.org/0000-0003-1340-4373>`_, primarily to meet our meshing needs. We developed qmesh with an open-source philosophy in mind, so you are welcome to view the qmesh code and use qmesh, under the `GNU General Public License, version 3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_. As the project grew and matured, the code was reorganised into various modules and packages. Following the traditional Python practice, each package is self-contained and includes documentation.  However, the documentation of the packages is best described as a technical reference. Therefore, we have developed this synoptic manual where we aim to provide a brief but complete description of the general architecture of qmesh, development practices, installation procedures, an overview of further documentation and literature as well as a few tutorials that will enable the novice to make a quick start.

In particular, this manual is organised as follows:

 *  The :ref:`Introduction` is a broad descriptions of the aims and objectives behind qmesh development.
 *  The :ref:`Architecture` is discussed in the second chapter, describing the user interfaces, the packages and the qmesh modules.
 *  The third chapter outlines the development and distribution methods, including a description of the testing framework and release practices. The chapter includes verbatim copies of the licences attached to qmesh code and documentation.
 *  Chapter four includes instructions on qmesh :ref:`Installation`.
 *  Chapter five outlines sources of :ref:`furtherDocumentation` and :ref:`tutorials`. Other literature sources, such as :ref:`academic_papers`, are also listed.

.. toctree::
   :maxdepth: 3

   introduction
   architecture
   developmentDistributionLicence
   installation
   tutorial
   furtherDocumentation

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

