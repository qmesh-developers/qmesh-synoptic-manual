.. This file is part of qmesh synoptic manual
   Copyright (C) 2017 Alexandros Avdis.
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _Installation:

Installation
==========================

Dependencies
--------------------------

qmesh relies on the following packages:

* QGIS
* gmsh
* GFD_basisChange_tools
* PyRDM
* setuptools-qmesh

Installing from PyPI
--------------------------

The easiest way to install qmesh and qmesh-cli is through the Python Package Index. To install the qmesh python API type the following at the command line:

.. code-block:: bash

   pip install qmesh

If your system has ``pip`` installed, the above command will install all dependencies themselves distributed through PyPI.
The install procedure will also check for gmsh and qgis and will halt if those are not installed.
In your system is missing qgis, it is best to install it through your distributions package manager as documented in the qmesh web-page.
If your system is missing gmsh, the best option is to obtain a compiled binary from the gmsh web-page.

To install the qmesh cli :

.. code-block:: bash

   pip install qmesh-cli

An advantage to installing via PyPI, is the ease to upgrade and remove.
Should you wish to upgrade, type the following at a terminal:

.. code-block:: bash

   pip install --upgrade SomePackage <qmesh-package>

To remove type the following:

.. code-block:: bash

   pip uninstall <qmesh-package>

Docker containers
--------------------------

Docker containers can be thought of as virtual machines, and provide isolation and control of a working environment.
It is easy to construct *docker images* where the operating system has all the necessary dependencies installed.
The images are typically made accessible by uploading to online *registries*.
`Docker hub <https://hub.docker.com/>`_ is a popular docker registry, where developers upload images, organised into *repositories*.
For example, the `qmesh repository <https://hub.docker.com/r/qmesh/qmesh-containers/>`_ houses all qmesh images, where each image is identified by a unique image *tag*.
Some images are intended for testing and development, and the environment is set-up with a minimal set of dependencies to provide a clean "sand-box".
Other images provide a full qmesh installation and are aimed at facilitating use of qmesh when a user's environment makes installation hard or impossible.
You can see all qmesh images in the `qmesh repository tags page <https://hub.docker.com/r/qmesh/qmesh-containers/tags/>`_.
Note that you must have docker installed on your system, preferably thought a package manager to use docker images.

However, since docker provides a user with a bare-bone system, it puts the task of system administration onto the container user.
Administering a Linux system can be demanding for users who just want to run qmesh, rather than spend time administering a system.
The package ``qmeshcontainers`` provides the command-line utility ``qmeshcontainer``, the latter is meant to facilitate creating and running qmesh containers. The package ``qmeshcontainers`` is available from the `Python Package Index https://pypi.org/project/qmeshcontainers/`_.

A complete description of the ``qmeshcontainer`` utility is available in the ``qmeshcontainers`` manual, a brief outline of installation and use is presented here.

To install ``qmeshcontainers`` issue the following at the command line:

.. code-block:: bash

   sudo pip install qmeshcontainers

The primary usage scenario of qmesh containers, is that the user will have prepared the input files in some way and want to run qmesh.
In the commands below, it is assumed that the input files are located in the directory ``mesh``. 

From the command line of your host (computer) navigate to the ``mesh`` directory and enter the directory.
To create and run a qmesh container issue the following at the command line:

.. code-block:: bash

   qmeshcontainer -mwd

The above will place you inside the qmesh container, giving you a Bash prompt.
If you see any warning messages about user or group details mismatch, pay attention to them, but you do not have to take any action.
As the ``qmeshcontainers`` utility is preparing the container, it tries to match your user set-up (including user and group IDs) so it can access and write files that match your profile into the ``mesh`` directory.
The warning messages are produced in case the utility cannot fully replicate your user set-up.
The above ``qmeshcontainer`` command will also make the directory ``mesh`` available inside the container.
Any changes or deletions you make to the ``mesh`` directory are propagated to the host, so pay attention to any file deletions or changes you might make.
Navigate into the ``mesh`` directory in the container bash prompt:

.. code-block:: bash

   cd mesh

Assuming that the qmesh mesh generation script is called ``mesh_generation`` the following command at the container bash prompt will execute the script:

.. code-block:: bash

   python mesh_generation

Review the qmesh script output to troubleshoot any problems and rerun the script to obtain the mesh you want.
You can leave the container running, edit the input files on the host machine, and return to the terminal window with the container bash prompt to re-run the meshing script.
Similarly, you can use gmsh or qgis on the host machine to visualise the mesh you have produced.
Once you have obtained the mesh you want you can exit the container by typing ``exit`` or with the keyboard shortcut ``Ctrl + d``.

For more information on the ``qmeshcontainer`` utility, including various user options, please consult the qmesh containers manual.
