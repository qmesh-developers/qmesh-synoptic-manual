qmesh synoptic manual
============================

|docs|

This is the source of the qmesh synoptic manual.

To deploy locally

1. clone this repository

2. cd local clone

3. 'virtualenv qmesh-synopsis-venv'

4. 'source qmesh-synopsis-venv/qmesh-synopsis-venv/bin/activate'

5. 'pip install -r requirements.txt'

6. 'make html', or 'make epub', etc

7. 'make clean' to clean-up.

.. |docs| image:: https://readthedocs.org/projects/qmesh-synoptic-manual/badge/?version=latest
    :alt: Documentation Status
    :scale: 100%
    :target: https://qmesh-synoptic-manual.readthedocs.io/en/latest/?badge=latest
